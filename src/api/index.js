import axios from 'axios'

const swapi = axios.create({
  baseURL: process.env.VUE_APP_MODEL_SWAPI_URL,
  timeout: 60000,
})

swapi.interceptors.request.use()

export {
  swapi
}
