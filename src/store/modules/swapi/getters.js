const data = state => state.data
const auth = state => state.auth
const people = state => state.people
const films = state => state.films

export default {
  data,
  auth,
  people,
  films
}
