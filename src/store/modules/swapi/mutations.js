import { da } from "date-fns/locale"

const SAVE_FILMS = (state, data) => {
  const oldData = localStorage.getItem('films') ? JSON.parse(localStorage.getItem('films')) : []
  data.map(c=>{
    oldData.push(c)
  })
  const newData = JSON.stringify(oldData)
  localStorage.setItem('films', newData)
  state.films = data
}
const SAVE_PEOPLE = (state, data) => {
  const oldData = localStorage.getItem('people') ? JSON.parse(localStorage.getItem('people')) : []
  data.results.map(c => {
    oldData.push(c)
  })

  const res = JSON.stringify(oldData)
  localStorage.setItem('people', res)
  state.people = oldData
}

const CLEAN_DATA = (state) => {
  state.people = []
  state.data = []
  state.films = []
}

const SAVE_DATA = (state, data) => {
  const oldData = localStorage.getItem('data') ? JSON.parse(localStorage.getItem('data')) : []
  oldData.push(data)
  const newData = JSON.stringify(oldData)
  localStorage.setItem('data', newData)
  state.data.push(data)
}


export default {
  SAVE_PEOPLE,
  SAVE_FILMS,
  CLEAN_DATA,
  SAVE_DATA
}
