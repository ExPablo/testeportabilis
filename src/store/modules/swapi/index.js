import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import consts from './consts'

const readOrDefault = (key, defaultValue) => {
  const value = localStorage.getItem(key)

  if (value === null) {
    return defaultValue
  }

  return JSON.parse(value)
}

const state = {
  data: readOrDefault('data',[]),
  people: readOrDefault('people',[]),
  films: readOrDefault('films',[]),
  auth:null
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
