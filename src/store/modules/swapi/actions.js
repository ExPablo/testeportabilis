import { swapi }  from '@/api'


const check = async context => {
    const cache = JSON.parse(localStorage.getItem('people'))
    if(cache){
      return true
    }else{
      return false
    }
}

const clear = async context => {
  localStorage.removeItem('data')
  localStorage.removeItem('people')
  localStorage.removeItem('films')
  context.commit('CLEAN_DATA')
  return true
}

const syncDataPageOne = async context => {
  try {
    const res = await swapi.get('people/?page=1')
    context.commit('SAVE_PEOPLE', res.data)
    return true
  }catch(err){
    return false
  }
}

const syncDataPageTwo = async context => {
  try {
    const res = await swapi.get('people/?page=2')
    context.commit('SAVE_PEOPLE', res.data)
    return true
  }catch(err){
    return false
  }
}

const syncFilms = async context => {
  try {
    const res = await swapi.get('films')
    context.commit('SAVE_FILMS', res.data.results)
    return true
  }catch(err){
    return false
  }
}

const getSpecies = async (context, data) => {
  let URL = data || 'http://swapi.dev/api/species/1/'
  try {
    const check = await swapi.get(URL)
    context.commit('SAVE_DATA', check.data)
    return check.data
  }catch(err){
    console.log(err, 'err')
    return false
  }
}

const getPlanet = async (context, data) => {
  try {
    const check = await swapi.get(data)
    context.commit('SAVE_DATA', check.data)
    return check.data
  }catch(err){
    console.log(err, 'err')
    return false
  }
}

const login = async (context, data) => {
  if(data.user == "sury" && data.pass == 123){
    localStorage.setItem('user', data.user);
    localStorage.setItem('pass', data.pass);
    return true
  }else{
    return false
  }
}


export default {
  login,
  getPlanet,
  check,
  syncDataPageOne,
  syncDataPageTwo,
  clear,
  getSpecies,
  syncFilms
}
