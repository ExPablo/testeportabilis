import Vue from 'vue'
import Vuex from 'vuex'
import swapi from './modules/swapi'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    swapi,
  }
})

export default store
